package nl.sdb.open.fuseki;

import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.eclipse.rdf4j.http.client.SPARQLProtocolSession;
import org.eclipse.rdf4j.http.protocol.Protocol;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.Dataset;
import org.eclipse.rdf4j.query.QueryLanguage;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class FusekiSparqlProtocolSession extends SPARQLProtocolSession {

    public FusekiSparqlProtocolSession(HttpClient client, ExecutorService executor) {
        super(client, executor);
    }

    protected void setQueryURL(String queryURL) {
        super.setQueryURL(queryURL);
    }

    protected void setUpdateURL(String updateURL) {
        super.setUpdateURL(updateURL);
    }

    protected HttpUriRequest getQueryMethod(QueryLanguage ql, String query, String baseURI, Dataset dataset,
                                            boolean includeInferred, int maxQueryTime, Binding... bindings) {
        List<NameValuePair> queryParams = getQueryMethodParameters(ql, query, baseURI, dataset, includeInferred,
                maxQueryTime, bindings);
        try {
            URIBuilder urib = new URIBuilder(getQueryURL());
            for (NameValuePair nvp : queryParams) {
                urib.addParameter(nvp.getName(), nvp.getValue());
            }
        } catch (URISyntaxException e) {
            throw new AssertionError(e);
        }
        // we just built up a URL for nothing. oh well.
        // It's probably not much overhead against
        // the poor triplestore having to process such as massive query

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("query", query));
        HttpPost postMethod = new HttpPost(getQueryURL());
        postMethod.setHeader(HttpHeaders.CONTENT_TYPE, Protocol.FORM_MIME_TYPE + "; charset=utf-8");
        postMethod.setEntity(new UrlEncodedFormEntity(params, UTF8));

        // functionality to provide custom http headers as required by the
        // applications
        for (Map.Entry<String, String> additionalHeader : super.getAdditionalHttpHeaders().entrySet()) {
            postMethod.addHeader(additionalHeader.getKey(), additionalHeader.getValue());
        }
        return postMethod;
    }

}
