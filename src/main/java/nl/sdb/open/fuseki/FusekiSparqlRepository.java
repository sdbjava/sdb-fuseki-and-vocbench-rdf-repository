package nl.sdb.open.fuseki;

import org.eclipse.rdf4j.http.client.HttpClientDependent;
import org.eclipse.rdf4j.http.client.HttpClientSessionManager;
import org.eclipse.rdf4j.http.client.SPARQLProtocolSession;
import org.eclipse.rdf4j.http.client.SessionManagerDependent;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.resultio.TupleQueryResultFormat;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sparql.SPARQLConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.eclipse.rdf4j.http.client.SharedHttpClientSessionManager.CORE_POOL_SIZE_PROPERTY;

public class FusekiSparqlRepository extends SPARQLRepository implements HttpClientDependent, SessionManagerDependent {
    private final static Logger LOGGER = LoggerFactory.getLogger(FusekiSparqlRepository.class);

    private final String queryEndpointUrl;
    private final String updateEndpointUrl;
    private final ExecutorService executor;
    private Map<String, String> additionalHttpHeaders;
    private String username;
    private String password;

    public FusekiSparqlRepository(String endpointUrl, String updateUrl) {
        super(endpointUrl, updateUrl);
        updateEndpointUrl = updateUrl;
        queryEndpointUrl = endpointUrl;
        final int corePoolSize = Integer.getInteger(CORE_POOL_SIZE_PROPERTY, 4);
        this.executor = Executors.newScheduledThreadPool(corePoolSize);
    }

    public FusekiSparqlRepository(String endpointUrl) {
        this(endpointUrl, endpointUrl);
    }

    public void setAdditionalHttpHeaders(Map<String, String> additionalHttpHeaders) {
        if (additionalHttpHeaders == null) {
            this.additionalHttpHeaders = Collections.emptyMap();
        } else {
            this.additionalHttpHeaders = additionalHttpHeaders;
        }
    }

    /**
     * Creates a new HTTPClient object. Subclasses may override to return a more specific HTTPClient subtype.
     *
     * @return a HTTPClient object.
     */
    protected SPARQLProtocolSession createHTTPClient() {
        // initialize HTTP client
        HttpClientSessionManager httpClientSessionManager = getHttpClientSessionManager();
        FusekiSparqlProtocolSession httpClientSession = new FusekiSparqlProtocolSession(httpClientSessionManager.getHttpClient(), executor);
        httpClientSession.setQueryURL(queryEndpointUrl);
        httpClientSession.setUpdateURL(updateEndpointUrl);
        httpClientSession.setValueFactory(SimpleValueFactory.getInstance());
        httpClientSession.setPreferredTupleQueryResultFormat(TupleQueryResultFormat.SPARQL);
        httpClientSession.setAdditionalHttpHeaders(additionalHttpHeaders);
        if (username != null) {
            httpClientSession.setUsernameAndPassword(username, password);
        }
        LOGGER.info("PreferredRDFFormat " + httpClientSession.getPreferredRDFFormat());
        LOGGER.info("PreferredBooleanQueryResultFormat " + httpClientSession.getPreferredBooleanQueryResultFormat());
        return httpClientSession;
    }

    public void setUsernameAndPassword(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public RepositoryConnection getConnection() throws RepositoryException {
        if (!super.isInitialized()) {
            super.init();
        }
        SPARQLProtocolSession httpClient = this.createHTTPClient();
        LOGGER.info("created " + httpClient);
        return new SPARQLConnection(this, httpClient);
    }


}

