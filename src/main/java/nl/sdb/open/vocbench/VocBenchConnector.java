package nl.sdb.open.vocbench;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.eclipse.rdf4j.http.client.HttpClientSessionManager;
import org.eclipse.rdf4j.http.client.SharedHttpClientSessionManager;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"MismatchedQueryAndUpdateOfStringBuilder", "StringBufferReplaceableByString"})
public class VocBenchConnector {
    private static VocBenchConnector _instance;
    private final CookieStore cookieStore = new BasicCookieStore();
    private final HttpClientBuilder builder;
    private final String loginUrl;
    private final String username;
    private final String password;

    private VocBenchConnector(String url, String username, String password) {
        this.loginUrl = url;
        this.username = username;
        this.password = password;
        this.builder = HttpClientBuilder.create();
        builder.setDefaultCookieStore(cookieStore);
        builder.setRedirectStrategy(new LaxRedirectStrategy());
    }

    public static VocBenchConnector getInstance(String url, String username, String password) {
        if (_instance == null) {
            _instance = new VocBenchConnector(url, username, password);
        }
        return _instance;
    }

    private void login() throws IOException {
        try (CloseableHttpClient httpClient = builder.build()) {
            StringBuilder url = new StringBuilder(loginUrl);
            url.append("?email=");
            url.append(username);
            url.append("&password=");
            url.append(password);
            url.append("&_spring_security_remember_me=true");
            HttpPost httpPost = new HttpPost(url.toString());

            try (CloseableHttpResponse closeableHttpResponse = httpClient.execute(httpPost)) {
                StatusLine statusLine = closeableHttpResponse.getStatusLine();
                if (statusLine.getStatusCode() != HttpStatus.SC_OK) {
                    throw new IOException(statusLine.getStatusCode() + " " + statusLine.getReasonPhrase());
                }
            }
        }
    }

    public CloseableHttpClient getHttpClient() throws IOException {
        List<Cookie> cookies = cookieStore.getCookies();
        if (cookies.isEmpty() || cookies.get(0).isExpired(new Date()) || cookies.get(1).isExpired(new Date())) {
            this.login();
        }

        builder.setDefaultCookieStore(cookieStore);
        return builder.build();
    }

    public HttpClientSessionManager getHttpClientSessionManager() throws IOException {
        List<Cookie> cookies = cookieStore.getCookies();
        if (cookies.isEmpty() || cookies.get(0).isExpired(new Date()) || cookies.get(1).isExpired(new Date())) {
            this.login();
        }
        SharedHttpClientSessionManager sharedHttpClientSessionManager = new SharedHttpClientSessionManager();
        sharedHttpClientSessionManager.setHttpClientBuilder(builder);
        return sharedHttpClientSessionManager;
    }

    /**
     * @param queryUrl url naar sparl endpoint
     * @return Repository manager
     * @throws IOException fout opgetreden
     * @deprecated
     */
    public RepositoryManager getRepositoryManager(String queryUrl) throws IOException {
        List<Cookie> cookies = cookieStore.getCookies();
        if (cookies.isEmpty() || cookies.get(0).isExpired(new Date()) || cookies.get(1).isExpired(new Date())) {
            this.login();
        }

        RepositoryManager repositoryManager = new RemoteRepositoryManager(queryUrl);

        builder.setDefaultCookieStore(cookieStore);
        repositoryManager.setHttpClient(builder.build());
        repositoryManager.init();

        return repositoryManager;
    }

    public String sparql(String sparqlQuery, String url, String collection, String format) throws IOException {

        List<Cookie> cookies = this.cookieStore.getCookies();
        if (cookies.isEmpty() || cookies.get(0).isExpired(new Date()) || cookies.get(1).isExpired(new Date())) {
            this.login();
        }
        try (CloseableHttpClient httpClient = builder.build()) {
            StringBuilder urlBuilder = new StringBuilder(url);
            urlBuilder.append("?ctx_project=");
            urlBuilder.append(collection);

            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("outputFormat", format));
            params.add(new BasicNameValuePair("query", sparqlQuery));
            params.add(new BasicNameValuePair("ctx_project", collection));
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            try (CloseableHttpResponse closeableHttpResponse = httpClient.execute(httpPost)) {
                StatusLine statusLine = closeableHttpResponse.getStatusLine();
                if (statusLine.getStatusCode() != HttpStatus.SC_OK) {
                    throw new IOException(statusLine.getStatusCode() + " " + statusLine.getReasonPhrase());
                }
                HttpEntity entity = closeableHttpResponse.getEntity();
                return EntityUtils.toString(entity, "UTF-8");
            }
        }
    }

    public Map<String, String> sendQueryViaHttp(HttpUriRequest request, HttpContext httpContext) throws IOException {
        List<Cookie> cookies = this.cookieStore.getCookies();
        if (cookies.isEmpty() || cookies.get(0).isExpired(new Date()) || cookies.get(1).isExpired(new Date())) {
            this.login();
        }
        Map<String, String> result = new HashMap<>();
        try (CloseableHttpClient httpClient = builder.build()) {
            CloseableHttpResponse httpResponse = httpClient.execute(request, httpContext);
            int httpCode = httpResponse.getStatusLine().getStatusCode();
            if (httpCode != HttpURLConnection.HTTP_OK && httpCode != HttpURLConnection.HTTP_NOT_AUTHORITATIVE) {
                // trying to contact a non-SPARQL server?
                throw new RepositoryException("Request failed with status " + httpCode + ": "
                        + request.getURI().toString());
            } else {
                result.put("content", EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8));
                result.put("Content-Type", httpResponse.getEntity().getContentType().getValue());
                return result;
            }
        }
    }

    public CookieStore getCookieStore() {
        return cookieStore;
    }
}

