package nl.sdb.open.vocbench;

import com.github.openjson.JSONObject;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.http.client.SPARQLProtocolSession;
import org.eclipse.rdf4j.http.protocol.Protocol;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.Dataset;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.resultio.BooleanQueryResultFormat;
import org.eclipse.rdf4j.query.resultio.BooleanQueryResultParser;
import org.eclipse.rdf4j.query.resultio.BooleanQueryResultParserRegistry;
import org.eclipse.rdf4j.query.resultio.QueryResultFormat;
import org.eclipse.rdf4j.query.resultio.QueryResultIO;
import org.eclipse.rdf4j.query.resultio.QueryResultParseException;
import org.eclipse.rdf4j.query.resultio.helpers.QueryResultCollector;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.springframework.util.MimeTypeUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;

public class VocBenchSparqlProtocolSession extends SPARQLProtocolSession {
    private final String ctxProject;
    private final VocBenchConnector vocBenchConnector;
    private final String askUrl;

    public VocBenchSparqlProtocolSession(ExecutorService executor, VocBenchConnector connector, String queryUrl, String askUrl, String collection) throws IOException {
        super(connector.getHttpClient(), executor);
        this.vocBenchConnector = connector;
        HttpClientContext httpContext = (HttpClientContext) this.getHttpContext();
        httpContext.setCookieStore(connector.getCookieStore());
        setQueryURL(queryUrl);

        this.ctxProject = collection;
        this.askUrl = askUrl;
    }

    protected HttpUriRequest getQueryMethod(QueryLanguage ql, String query, String baseURI, Dataset dataset,
                                            boolean includeInferred, int maxQueryTime, Binding... bindings) {
        List<NameValuePair> queryParams = getQueryMethodParameters(ql, query, baseURI, dataset, includeInferred,
                maxQueryTime, bindings);
        String queryUrlWithParams;
        try {
            URIBuilder urib = new URIBuilder((query.startsWith("ASK ")) ? askUrl : getQueryURL());

            if (urib.getQueryParams().isEmpty())
                urib.addParameter("ctx_project", ctxProject);
            queryUrlWithParams = urib.toString();
        } catch (URISyntaxException e) {
            throw new AssertionError(e);
        }

        // we just built up a URL for nothing. oh well.
        // It's probably not much overhead against
        // the poor triplestore having to process such as massive query

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("query", query));
        params.add(new BasicNameValuePair("ctx_project", ctxProject));
        if (query.startsWith("ASK ")) {
            params.add(new BasicNameValuePair("includeInferred", "false"));
        } else {
            params.add(new BasicNameValuePair("outputFormat", "Turtle"));
        }
        HttpPost postMethod = new HttpPost(queryUrlWithParams);
        postMethod.setHeader(HttpHeaders.CONTENT_TYPE, Protocol.FORM_MIME_TYPE + "; charset=utf-8");
        postMethod.setEntity(new UrlEncodedFormEntity(params, UTF8));

        // functionality to provide custom http headers as required by the
        // applications
        for (Map.Entry<String, String> additionalHeader : super.getAdditionalHttpHeaders().entrySet()) {
            postMethod.addHeader(additionalHeader.getKey(), additionalHeader.getValue());
        }
        return postMethod;
    }

    protected void setQueryURL(String queryURL) {
        super.setQueryURL(queryURL);
    }

    protected void setUpdateURL(String updateURL) {
        super.setUpdateURL(updateURL);
    }

    protected boolean getBoolean(HttpUriRequest method) throws IOException, RDF4JException {
        // Specify which formats we support using Accept headers
        Set<QueryResultFormat> booleanFormats = BooleanQueryResultParserRegistry.getInstance().getKeys();
        if (booleanFormats.isEmpty()) {
            throw new RepositoryException("No boolean query result parsers have been registered");
        }
        Map<String, String> viaHttp = vocBenchConnector.sendQueryViaHttp(method, super.getHttpContext());

        // if we get here, HTTP code is 200
        String mimeType = viaHttp.get("Content-Type");
        try {
            // het resultaat is json: {"result":{"resultType":"boolean","sparql":{"head":{},"boolean":true}}}
            Optional<QueryResultFormat> queryResultFormat = BooleanQueryResultFormat.matchMIMEType(mimeType, booleanFormats);

            if (queryResultFormat.isPresent()) {
                BooleanQueryResultParser parser = QueryResultIO.createBooleanParser(queryResultFormat.get());
                QueryResultCollector results = new QueryResultCollector();
                parser.setQueryResultHandler(results);
                parser.parseQueryResult(new ByteArrayInputStream(viaHttp.get("content").getBytes(StandardCharsets.UTF_8)));
                return results.getBoolean();
            } else if (mimeType.startsWith(MimeTypeUtils.APPLICATION_JSON_VALUE)) {
                // use org.apache.http.util.EntityUtils to read json as string
                String json = viaHttp.get("content");
                JSONObject o = new JSONObject(json);
                return o.getJSONObject("result").getJSONObject("sparql").getBoolean("boolean");
            } else {
                throw new RepositoryException("Server responded with an unsupported file format: " + mimeType);
            }

        } catch (QueryResultParseException e) {
            throw new RepositoryException("Malformed query result from server", e);
        }
    }

}
