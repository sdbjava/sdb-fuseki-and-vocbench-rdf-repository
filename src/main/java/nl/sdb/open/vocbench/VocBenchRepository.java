package nl.sdb.open.vocbench;

import org.apache.http.HttpHeaders;
import org.eclipse.rdf4j.http.client.HttpClientDependent;
import org.eclipse.rdf4j.http.client.HttpClientSessionManager;
import org.eclipse.rdf4j.http.client.SPARQLProtocolSession;
import org.eclipse.rdf4j.http.client.SessionManagerDependent;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.resultio.TupleQueryResultFormat;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sparql.SPARQLConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.eclipse.rdf4j.http.client.SharedHttpClientSessionManager.CORE_POOL_SIZE_PROPERTY;

public class VocBenchRepository extends SPARQLRepository implements HttpClientDependent, SessionManagerDependent {

    private final ExecutorService executor;
    private final VocBenchConnector connector;
    private final String queryUrl;
    private final String askUrl;
    private String ctxProject;

    public VocBenchRepository(String endpointUrl, VocBenchConnector connector, String askUrl, String project) {
        super(endpointUrl);
        this.askUrl = askUrl;
        this.queryUrl = endpointUrl;
        this.connector = connector;
        this.ctxProject = project;

        final int corePoolSize = Integer.getInteger(CORE_POOL_SIZE_PROPERTY, 4);
        this.executor = Executors.newScheduledThreadPool(corePoolSize);
    }

    public void setCtxProject(String ctxProject) {
        this.ctxProject = ctxProject;
    }

    private VocBenchSparqlProtocolSession createVocBenchHTTPClient() {
        HttpClientSessionManager httpClientSessionManager = getHttpClientSessionManager();
        // initialize HTTP client
        try (VocBenchSparqlProtocolSession httpClientSession = new VocBenchSparqlProtocolSession(executor, connector, queryUrl, askUrl, ctxProject)) {
            httpClientSession.setValueFactory(SimpleValueFactory.getInstance());
            httpClientSession.setPreferredTupleQueryResultFormat(TupleQueryResultFormat.SPARQL);

            httpClientSession.setQueryURL(queryUrl);
            httpClientSession.setUpdateURL(queryUrl);

            httpClientSession.setValueFactory(SimpleValueFactory.getInstance());
            httpClientSession.setPreferredTupleQueryResultFormat(TupleQueryResultFormat.SPARQL);
            return httpClientSession;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public RepositoryConnection getConnection() throws RepositoryException {
        if (!super.isInitialized()) {
            super.init();
        }
        SPARQLProtocolSession httpClient = this.createVocBenchHTTPClient();
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.ACCEPT, "application/json");
        httpClient.setAdditionalHttpHeaders(headers);
        return new SPARQLConnection(this, httpClient);
    }


}

