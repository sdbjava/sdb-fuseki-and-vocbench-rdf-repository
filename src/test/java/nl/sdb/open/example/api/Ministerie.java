package nl.sdb.open.example.api;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;
import org.cyberborean.rdfbeans.annotations.RDFNamespaces;
import org.cyberborean.rdfbeans.annotations.RDFSubject;

import java.net.URI;
import java.util.Collections;
import java.util.List;

/**
 * <p>De lijst bevast ook historische versie.
 * Die hebben een afwijkende id. Een normale id is:
 * https://identifier.overheid.nl/tooi/id/ministerie/mnre1058</p>
 * <p>
 * Er zijn verschillende type wijzigingen:</p>
 * <ul>
 * <li>
 * tooiont:/Toestandswijziging
 * </li><li>
 * tooiont:/Samenvoeging
 * </li><li>
 * tooiont:/Afsplitsing
 * </li>
 * </ul>
 *
 * <h2>Voorbeeld met predicaten en objecten</h2>
 * <p>Het Subject is <code>&lt;https://identifier.overheid.nl/tooi/id/ministerie/wzg_06434036&gt;</code>
 * </p>
 * <table summary="the owls are not as they seem" border="2">
 *
 *     <tr>
 * <td>&lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#type&gt;  </td><td>   tooiont:/Samenvoeging</td>
 * </tr><tr>
 * <td>&lt;http://www.w3.org/ns/prov#invalidated&gt; </td><td>  &lt;https://identifier.overheid.nl/tooi/id/ministerie/mnre1040&gt;</td>
 * </tr><tr>
 * <td>&lt;http://www.w3.org/ns/prov#invalidated&gt; </td><td> &lt;https://identifier.overheid.nl/tooi/id/ministerie/mnre1150&gt;</td>
 * </tr><tr>
 * <td>tooiont:/heeftGrondslagString </td><td> "https://zoek.officielebekendmakingen.nl/stcrt-2010-16584.html"</td>
 * </tr><tr>
 * <td>tooiont:/tijdstipWijziging </td><td> "2010-10-14T00:00:00"^^xsd:dateTime</td>
 * </tr><tr>
 * <td>&lt;http://www.w3.org/ns/prov#generated&gt; </td><td> &lt;https://identifier.overheid.nl/tooi/id/ministerie/mnre1045&gt;</td></tr>
 * </table>
 */
@RDFNamespaces({
        "tooiont = https://identifier.overheid.nl/tooi/def/ont/",
        "ministerie=http://identifier.overheid.nl/tooi/id/ministerie"
})
@RDFBean("tooiont:Ministerie")
public class Ministerie implements Comparable<Ministerie> {

    private final List<URI> used = Collections.emptyList();
    private String organisatiecode;
    private String id;
    private URI rdfType;
    private String owmsId;
    private String officieleNaamSorteer;
    private String officieleNaamExclSoort;
    private String einddatum;
    private String voorkeursnaamExclSoort;
    private String label;
    private String ministeriecode;
    private URI organisatiesoort;
    private String voorkeursnaamSorteer;
    private String officieleNaamInclSoort;
    private String voorkeursnaamInclSoort;
    private String afkorting;
    private String begindatum;

    @RDFSubject // (prefix = "prov:")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/owmsId")
    public String getOwmsId() {
        return owmsId;
    }

    public void setOwmsId(String owmsId) {
        this.owmsId = owmsId;
    }

    @RDF("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
    public URI getRdfType() {
        return rdfType;
    }

    public void setRdfType(URI rdfType) {
        this.rdfType = rdfType;
    }

    public String toString() {
        return new ReflectionToStringBuilder(this).toString();
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/organisatiecode")
    public String getOrganisatiecode() {
        return organisatiecode;
    }

    public void setOrganisatiecode(String organisatiecode) {
        this.organisatiecode = organisatiecode;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/officieleNaamSorteer")
    public String getOfficieleNaamSorteer() {
        return officieleNaamSorteer;
    }

    public void setOfficieleNaamSorteer(String officieleNaamSorteer) {
        this.officieleNaamSorteer = officieleNaamSorteer;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/officieleNaamExclSoort")
    public String getOfficieleNaamExclSoort() {
        return officieleNaamExclSoort;
    }

    public void setOfficieleNaamExclSoort(String officieleNaamExclSoort) {
        this.officieleNaamExclSoort = officieleNaamExclSoort;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/begindatum")
    public String getBegindatum() {
        return begindatum;
    }

    public void setBegindatum(String begindatum) {
        this.begindatum = begindatum;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/einddatum")
    public String getEinddatum() {
        return einddatum;
    }

    public void setEinddatum(String einddatum) {
        this.einddatum = einddatum;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/voorkeursnaamExclSoort")
    public String getVoorkeursnaamExclSoort() {
        return voorkeursnaamExclSoort;
    }

    public void setVoorkeursnaamExclSoort(String voorkeursnaamExclSoort) {
        this.voorkeursnaamExclSoort = voorkeursnaamExclSoort;
    }

    @RDF("http://www.w3.org/2000/01/rdf-schema#label")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/ministeriecode")
    public String getMinisteriecode() {
        return ministeriecode;
    }

    public void setMinisteriecode(String ministeriecode) {
        this.ministeriecode = ministeriecode;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/organisatiesoort")
    public URI getOrganisatiesoort() {
        return organisatiesoort;
    }

    public void setOrganisatiesoort(URI organisatiesoort) {
        this.organisatiesoort = organisatiesoort;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/voorkeursnaamSorteer")
    public String getVoorkeursnaamSorteer() {
        return voorkeursnaamSorteer;
    }

    public void setVoorkeursnaamSorteer(String voorkeursnaamSorteer) {
        this.voorkeursnaamSorteer = voorkeursnaamSorteer;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/officieleNaamInclSoort")
    public String getOfficieleNaamInclSoort() {
        return officieleNaamInclSoort;
    }

    public void setOfficieleNaamInclSoort(String officieleNaamInclSoort) {
        this.officieleNaamInclSoort = officieleNaamInclSoort;
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/voorkeursnaamInclSoort")
    public String getVoorkeursnaamInclSoort() {
        return voorkeursnaamInclSoort;
    }

    public void setVoorkeursnaamInclSoort(String voorkeursnaamInclSoort) {
        this.voorkeursnaamInclSoort = voorkeursnaamInclSoort;
    }

    @Override
    public int compareTo(Ministerie o) {
        return this.officieleNaamSorteer.compareTo(o.officieleNaamSorteer);
    }

    @RDF("https://identifier.overheid.nl/tooi/def/ont/afkorting")
    public String getAfkorting() {
        return afkorting;
    }

    public void setAfkorting(String afkorting) {
        this.afkorting = afkorting;
    }


}
