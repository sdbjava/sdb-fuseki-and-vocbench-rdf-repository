package nl.sdb.open.example.config;

import nl.sdb.open.vocbench.VocBenchConnector;
import nl.sdb.open.vocbench.VocBenchRepository;
import org.eclipse.rdf4j.http.client.HttpClientSessionManager;
import org.eclipse.rdf4j.repository.Repository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;


@Configuration
public class VocBenchRepoConfig {
    @org.springframework.beans.factory.annotation.Value("${vocbench_login_url")
    private String vocBenchLoginUrl;
    @org.springframework.beans.factory.annotation.Value("${vocbench_user}")
    private String vocbenchUser;
    @org.springframework.beans.factory.annotation.Value("${vocbench_password}")
    private String vocBenchPwd;
    @org.springframework.beans.factory.annotation.Value("${vocbench_query_url}")
    private String vocbenchQueryUrl;
    @org.springframework.beans.factory.annotation.Value("${vocbench_ask_url:}")
    private String askUrl;

    private String project;

    public void setProject(String project) {
        this.project = project;
    }

    @Bean
    public Repository getRepository() throws IOException {
        VocBenchConnector instance = VocBenchConnector.getInstance(vocBenchLoginUrl, vocbenchUser, vocBenchPwd);
        HttpClientSessionManager httpClientSessionManager = instance.getHttpClientSessionManager();
        VocBenchRepository repository = new VocBenchRepository(vocbenchQueryUrl, instance, askUrl, project);
        repository.setHttpClientSessionManager(httpClientSessionManager);
        return repository;
    }
}
