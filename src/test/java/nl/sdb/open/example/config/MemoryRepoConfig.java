package nl.sdb.open.example.config;


import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class MemoryRepoConfig {

    @Bean
    public Repository getRepository() throws IOException {
        String fileName = "ministerie.ttl";
        // Create a new Repository.
        Repository repository = new SailRepository(new MemoryStore());
        repository.init();

        // Open a connection to the database
        try (RepositoryConnection conn = repository.getConnection();
             InputStream input = this.getClass().getClassLoader().getResourceAsStream(fileName)) {
            // add the RDF data from the inputstream directly to our database
            conn.add(input, "", RDFFormat.TURTLE);
        }
        return repository;
    }

}
