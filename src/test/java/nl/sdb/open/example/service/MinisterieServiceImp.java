package nl.sdb.open.example.service;

import nl.sdb.open.example.api.Ministerie;
import org.cyberborean.rdfbeans.RDFBeanManager;
import org.cyberborean.rdfbeans.RDFBeanManagerContext;
import org.cyberborean.rdfbeans.exceptions.RDFBeanException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MinisterieServiceImp implements MinisterieService {

    private final SimpleValueFactory VF = SimpleValueFactory.getInstance();
    @Autowired
    protected Repository repository;

    RDFBeanManager loadManager(RepositoryConnection con) throws RDFBeanException {
        RDFBeanManager manager = new RDFBeanManager(con.getRepository());
        String prefix = "https://identifier.overheid.nl/tooi/def/ont/Ministerie";
        manager.get(con.getValueFactory().createIRI(prefix), Ministerie.class);
        return manager;
    }


    protected RepositoryConnection getRepositoryConnection() {
        return repository.getConnection();
    }

    @Override
    public Resource update(Ministerie r) throws RDFBeanException {
        try (RepositoryConnection con = getRepositoryConnection()) {
            RDFBeanManager manager = loadManager(con);
            IRI context = manager.getContext();
            RDFBeanManagerContext rdfBeanManagerContext = manager.getContext(context);
            return rdfBeanManagerContext.update(r);
        }
    }

    @Override
    public Ministerie getWaarde(String uri) throws RDFBeanException {
        try (RepositoryConnection con = getRepositoryConnection()) {
            RDFBeanManager manager = loadManager(con);
            return manager.get(uri, Ministerie.class);
        }
    }

    @Override
    public Resource add(Ministerie r) throws RDFBeanException {
        try (RepositoryConnection con = getRepositoryConnection()) {
            RDFBeanManager manager = loadManager(con);
            return manager.add(r);
        }
    }
}

