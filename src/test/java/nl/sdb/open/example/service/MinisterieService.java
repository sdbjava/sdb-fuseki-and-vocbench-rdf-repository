package nl.sdb.open.example.service;

import nl.sdb.open.example.api.Ministerie;
import org.cyberborean.rdfbeans.exceptions.RDFBeanException;
import org.eclipse.rdf4j.model.Resource;

import java.io.IOException;

public interface MinisterieService {
    Resource update(Ministerie r) throws RDFBeanException, IOException;

    Ministerie getWaarde(String uri) throws RDFBeanException, IOException;

    Resource add(Ministerie r) throws RDFBeanException, IOException;
}
