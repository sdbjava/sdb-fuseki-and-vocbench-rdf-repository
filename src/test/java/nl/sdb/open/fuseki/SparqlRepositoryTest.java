package nl.sdb.open.fuseki;

import nl.sdb.open.example.api.Ministerie;
import nl.sdb.open.example.config.MemoryRepoConfig;
import nl.sdb.open.example.service.MinisterieService;
import nl.sdb.open.example.service.MinisterieServiceImp;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.cyberborean.rdfbeans.exceptions.RDFBeanException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Unfortunately this does not use the realthing, but just an in Memaory Repository for demonstration purposes
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {MemoryRepoConfig.class, MinisterieServiceImp.class, MinisterieService.class})
class SparqlRepositoryTest {
    @Autowired
    private MinisterieService ministerieService;

    @Test
    void getWaarde() throws RDFBeanException, IOException {
        String uriMinisterie = "https://identifier.overheid.nl/tooi/id/ministerie/mnre0180";
        Ministerie ministerie = ministerieService.getWaarde(uriMinisterie);
        assertNotNull(ministerie);
        assertEquals(uriMinisterie, ministerie.getId());
        System.out.println(ministerie);
        assertEquals("ministerie van Volkshuisvesting, Ruimtelijke Ordening en Milieubeheer", ministerie.getLabel());
        System.out.println(new ReflectionToStringBuilder(ministerie, ToStringStyle.MULTI_LINE_STYLE));
    }
}