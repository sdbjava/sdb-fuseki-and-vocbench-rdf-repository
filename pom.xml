<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>nl.sdb.open</groupId>
    <artifactId>sdb_repos</artifactId>
    <packaging>jar</packaging>
    <version>1.0.9-SNAPSHOT</version>

    <name>sdb_repos</name>
    <description>VocBench and Fuseki repository for eclipse rdf</description>
    <inceptionYear>2020</inceptionYear>
    <organization>
        <name>SDB / Hinttech</name>
        <url>www.sdb.nl</url>
    </organization>

    <properties>
        <java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <rdf.repository.version>3.4.0</rdf.repository.version>
    </properties>

    <scm>
        <connection>scm:git:git@bitbucket.org:sdbjava/sdb-fuseki-and-vocbench-rdf-repository.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:sdbjava/sdb-fuseki-and-vocbench-rdf-repository.git
        </developerConnection>
        <url>https://bitbucket.org/sdbjava/sdb-fuseki-and-vocbench-rdf-repository.git</url>
        <tag>HEAD</tag>
    </scm>
    <distributionManagement>
        <repository>
            <id>fakerepo</id>
            <name>fakerepo</name>
            <url>${fakerepo.url}</url>
        </repository>
        <!--        <snapshotRepository>-->
        <!--            <id>ossrh</id>-->
        <!--            <url>https://oss.sonatype.org/content/repositories/snapshots</url>-->
        <!--        </snapshotRepository>-->
        <!--        <repository>-->
        <!--            <id>ossrh</id>-->
        <!--            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>-->
        <!--        </repository>-->
    </distributionManagement>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>8</source>
                    <target>8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.2.0</version>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
<!--            <plugin>-->
<!--                <groupId>org.sonatype.plugins</groupId>-->
<!--                <artifactId>nexus-staging-maven-plugin</artifactId>-->
<!--                <version>1.6.8</version>-->
<!--                <extensions>true</extensions>-->
<!--                <configuration>-->
<!--                    <serverId>ossrh</serverId>-->
<!--                    <nexusUrl>https://oss.sonatype.org/</nexusUrl>-->
<!--                    <autoReleaseAfterClose>true</autoReleaseAfterClose>-->
<!--                </configuration>-->
<!--            </plugin>-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>1.6</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>org.eclipse.rdf4j</groupId>
            <artifactId>rdf4j-repository-api</artifactId>
            <version>${rdf.repository.version}</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.rdf4j</groupId>
            <artifactId>rdf4j-repository-manager</artifactId>
            <version>${rdf.repository.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>5.2.6.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>com.github.openjson</groupId>
            <artifactId>openjson</artifactId>
            <version>1.0.12</version>
        </dependency>

        <!-- Test and example dependencies -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.rdf4j</groupId>
            <artifactId>rdf4j-sail-memory</artifactId>
            <version>${rdf.repository.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.cyberborean</groupId>
            <artifactId>rdfbeans</artifactId>
            <version>2.3.0</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>5.5.2</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.rdf4j</groupId>
            <artifactId>rdf4j-repository-sail</artifactId>
            <version>${rdf.repository.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.rdf4j</groupId>
            <artifactId>rdf4j-repository-sparql</artifactId>
            <version>${rdf.repository.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.11</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>5.2.8.RELEASE</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-test</artifactId>
            <version>2.2.5.RELEASE</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>5.2.4.RELEASE</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <developers>
        <developer>
            <email>j.heijning@sdb.nl</email>
            <name>Jeroen Heijning</name>
            <id>jheyning</id>
            <roles>
                <role>developer</role>
            </roles>
            <organization>SDB Hinttech</organization>
            <organizationUrl>http://sdbjava.nl</organizationUrl>
        </developer>
    </developers>
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>2.7</version>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>index</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.2.0</version>
                <reportSets>
                    <reportSet><!-- by default, id = "default" -->
                        <reports><!-- select non-aggregate reports -->
                            <report>javadoc</report>
                            <report>test-javadoc</report>
                        </reports>
                    </reportSet>
                    <reportSet><!-- aggregate reportSet, to define in poms having modules -->
                        <id>aggregate</id>
                        <inherited>false</inherited><!-- don't run aggregate in child modules -->
                        <reports>
                            <report>aggregate</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>findbugs-maven-plugin</artifactId>
                <version>3.0.5</version>
                <configuration>
                    <effort>Max</effort>
                    <threshold>Medium</threshold>
                </configuration>
            </plugin>
        </plugins>
    </reporting>
</project>
