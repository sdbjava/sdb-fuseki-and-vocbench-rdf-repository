# SDB Sparql Repositories for Fuseki and VocBench

## SDB
SDB  means Software Detacherings Bureau
It is a Software company foundedin 1989 from an attic in a student town and still going strong. 
see: http://sdbjava.nl/over-sdb-java/
But do not believe what they say about Krakau.

## Fuseki
Fuseki is an open source triple store.
Fuseki requires that the SPARQL query is passed to the Feki server as a request parameter, not a URL parameter.
By overriding the eclipse rdf SPARQLRepository class this issue can be fixed fairly easily.

## VocBench
VocBench is a triple store, founded by the University of Rome. 
For VocBench more is needed to allow it to work.

### Reference Documentation

### What is this repository for? ###

The purpose of the library is to be able to use the cyberborean library to access values and subjects form a triple store using annotations.
This can be done by using @RDFBean en @RDF.
See: https://rdfbeans.github.io/


#### Example Configuration for VocBench

See test case

 
 ### Who do I talk to? ###
 
 * Repo owner J. heijning at j.heijning@sdb.nl
 
